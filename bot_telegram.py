import asyncio
import time
import requests
from web3 import Web3
from web3.middleware import geth_poa_middleware
from telegram import Bot

# Cấu hình thông tin truy cập BscScan
bscscan_api_key = 'Z72KCGBPXVBBQ66WD7M3JXTN21I5W86QTJ'
bscscan_url = 'https://api.bscscan.com/api'

# Cấu hình thông tin bot Telegram
telegram_bot_token = '6105810597:AAFOG59_9-32GmIO_XKvKiNMU-I94GcDzSk'
telegram_chat_id = '-924476347'

# Kết nối đến mạng Binance Smart Chain
web3 = Web3(Web3.HTTPProvider('https://bsc-dataseed.binance.org/'))

# Thêm middleware POA để xử lý đúng loại mạng
web3.middleware_onion.inject(geth_poa_middleware, layer=0)

# Tạo đối tượng bot Telegram
bot = Bot(token=telegram_bot_token)


async def send_message_to_telegram(message):
    await bot.send_message(chat_id=telegram_chat_id, text=message)


async def listen_transfer_event(wallet_address):
    while True:
        # Lấy thông tin về các giao dịch chuyển token
        url = f"{bscscan_url}?module=account&action=tokentx&address={wallet_address}&startblock=0&endblock=999999999&sort=desc&apikey={bscscan_api_key}"
        response = requests.get(url)
        if response.status_code == 200:
            transactions = response.json()['result']
            for tx in transactions:
                from_address = tx['from']
                to_address = tx['to']
                value = float(tx['value']) / 1e18  # Chuyển đổi giá trị về đơn vị đúng
                symbol = tx['tokenSymbol']
                message = f"Transfer: {from_address} -> {to_address}\nAmount: {value} {symbol}"
                await send_message_to_telegram(message)

        # Ngủ 5 giây trước khi kiểm tra lại
        time.sleep(5)


async def main():
    wallet_address = '0xd4ae6eca985340dd434d38f470accce4dc78d109'

    # Gửi một tin nhắn test đến nhóm Telegram
    await send_message_to_telegram("Hello, this is a test message!")

    # Lắng nghe sự kiện chuyển token
    await listen_transfer_event(wallet_address)

if __name__ == '__main__':
    asyncio.run(main())
